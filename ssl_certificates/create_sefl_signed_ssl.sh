#!/bin/bash

# this script creates a self-signed ssl-certificate for the domain *.core.fincite.net
# without any user interaction. 
# Usefull for automated test deployments


# creating a configuration file for openssl
cat > certificate-request.conf <<EOL;
[req]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn
[ dn ]
C="DE"
ST="Hessen"
L="Frankfurt"
O="Fincite"
OU="IT Infrastructure"
CN="*.core.fincite.net"
emailAddress="hostmaster@fincite.de"
[ req_ext ]
subjectAltName = @alt_names
[ alt_names ]
DNS.1 = "*.core.fincite.net"
#DNS.2 = ###alternative domain name, more can be add, e.g. "www.financemanager.trumpf.com"###
EOL

# create a new self-signed certificate based on the config file created above. Output in the same directory privkey.pem and fullchain.pem
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout privkey.pem -out fullchain.pem -config certificate-request.conf