#Project structure
* Inventory files has format: inventory_{{env}}
* Playbook files has format: {{id}}\_{{server name}}

#Execute command
The follwing command will do the installation for:

* Core.Connect 
* Core.Interact
* Core.Analyse2
* Distributing wildcard ssl certs from tin-test server to application servers

`ansible-playbook -i inventory_Dev1 ../../Git/ansible_cores_ssl/distributing_ssl_key.yml 001_dbservers.yml 002_connectservers.yml 003_interactservers.yml 004_analyse2servers.yml analyse1_playbook.yml`

#Additional Configuration
##Core.Analyse1
Databases and database user creation is not included in the playbook. So, a manual additional step is required. These 2 file need to be executed with: 

`sudo mysql`

`mysql> source file.sql`

* /home/cloud-user/core-analyse/algos/portfolio_healthcheck/Database/PH_user.sql
* /home/cloud-user/core-analyse/algos/portfolio_healthcheck/Database/PH_schema.sql
Add the additional lines for PH_schema.sql:
`CREATE DATABASE  IF NOT EXISTS currencies;`

There is one setup file need to be run manually: `"{{project_path}}/scripts/innovo-setup.sh"`

#Requirements for pipeline
* public ssh key of the pipeline vm should be added to the following bitbucket repo:
 * core.connect
 * core.interact
 * core.analyse2
* application server should allow ssh connection from the vm


#Requirements for local run

* .ssh/config should be configured so that ssh is possible with ip addresses in inventory file

#Process for setting up new pipeline
* Provisoning should be possible on local env (development done)
* Enable pipeline on the desired repository and adding pipeline setting file to the repo
* Adding secrets variables to pipeline environment variables
* Generating and copy public ssh key of the pipeline vm to Operations@fincite.de bitbucket account. This will grant the pipeline vm machine access to all required private repositories.
* Adding pipeline puplic key to the target server to give it ssh access
* Test by make some changes to tracked branch

Notice:
* Pipeline need ssh config that provided in this repo (can be found at ssh_config folder)

