# service_api.conf

# the upstream component nginx needs to connect to
# I comment-out this upstream service because it is already defined in the http vhost
upstream algo_service_api {
    server unix://{{ socket_folder }}/socketfile.sock;
}

# configuration of the server
server {
    listen [::]:80;
    listen 80;

    server_tokens off;

    # the domain name it will serve for
    server_name {{ nginx_server_name }};

    charset     utf-8;

    # max upload size
    client_max_body_size 75M;   # adjust to taste

    # Django media
    location /media  {
        alias {{ nginx_media_dir }};  # your Django project's media files - amend as required
    }

    location /static {
        alias {{ nginx_static_dir }}; # your Django project's static files - amend as required
    }

    # Finally, send all non-media requests to the Django server.
    location / {
  
        uwsgi_pass  algo_service_api;
        include     {{ uwsgi_params_path }}/uwsgi_params;

        # Stefan, 17.04.2017
        # Connection to Core.Analyse can take longer and a higher timeout than 60s is needed
        # These numbers and timeout commands needs to be reviewed!!
        uwsgi_read_timeout 300;
        

    }

    # Logs
    access_log {{ application_log_dir }}/nginx/access.log;
    error_log {{ application_log_dir }}/nginx/error.log;

    # Error page
    error_page 500 502 /maintenance.html;
    location = /maintenance.html {
        root /var/www/;
    }
}