#!/bin/sh

sumfile={{application_path}}/script/md5list
path={{project_path}}/ftp/thomsonreuters_lipperftp/DE/

if [ ! $path ]; then
  echo "I need path input!"
  exit 1
fi

# Generate sumfile if not exist, first run will be fine
if [ ! -f "$sumfile" ]; then
  echo $(date) "Generate sumfile"
  find $path -type f -name '*.zip' -exec md5sum {} \; > $sumfile
fi

# Loop through every *.zip file in path and check
find $path -type f -name '*.zip' | while read line; do
    newsum="`md5sum $line`"

    if grep -q "$newsum" $sumfile; then
      echo "$line matched"
    else
      echo $(date) $line "unmatched"
      #Run Django command
      {{application_path}}/python_env/bin/python {{application_path}}/core_analyse2/core_analyse2/manage.py tr_data_import {{project_path}}/ftp/thomsonreuters_lipperftp/DE/
      #Generate new hash keys list
      find $path -type f -name '*.zip' -exec md5sum {} \; > $sumfile
      exit 2
    fi
done

echo "\n" $(date) "Last run" "\n"
exit 3